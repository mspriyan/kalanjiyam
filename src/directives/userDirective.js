import {
  SchemaDirectiveVisitor,
  AuthenticationError
} from "apollo-server-express";
import { defaultFieldResolver } from "graphql";

class UserDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition(field) {
    const { resolve = defaultFieldResolver } = field;
    field.resolve = async function(...args) {
      const [, , context] = args;
      if (!context.user) {
        throw new AuthenticationError("You must login first.");
      } else {
        return await resolve.apply(this, args);
      }
    };
  }
}

export default UserDirective;
