import express from "express";
import { ApolloServer } from "apollo-server-express";
import mongoose from "mongoose";
import cors from "cors";
import jwt from "express-jwt";
import typeDefs from "./typeDefs";
import resolvers from "./resolvers";
import { default as schemaDirectives } from "./directives";
import { hostname } from "os";

const {
  SECRET,
  NODE_ENV,
  PORT,
  DB_USERNAME,
  DB_PASSWORD,
  DB_HOST,
  DB_PORT,
  DB_NAME
} = process.env;

const app = express();

app.use(cors());

app.disable("x-powered-by");

const auth = jwt({
  secret: SECRET,
  credentialsRequired: false
});

app.use(auth);

(async () => {
  try {
    await mongoose.connect(
      `mongodb://${DB_USERNAME}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}`,
      { useNewUrlParser: true }
    );

    const server = new ApolloServer({
      // These will be defined for both new or existing servers
      typeDefs,
      resolvers,
      context: ({ req }) => {
        const user = req.user;
        return { user };
      },
      schemaDirectives,
      playground: true,
      introspection: true
    });

    server.applyMiddleware({ app }); // app is from an existing express app

    app.get("/", function(req, res) {
      res.redirect("/graphql");
    });

    app.listen({ port: PORT }, () =>
      console.log(
        `Server ready is ready at http://${hostname}:${PORT}${
          server.graphqlPath
        }`
      )
    );
  } catch (error) {
    console.log(error);
  }
})();
