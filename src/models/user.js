import mongoose, { Schema } from "mongoose";
import { hash, compare } from "bcryptjs";

const userSchema = new Schema({
  name: {
    type: String,
    validate: {
      validator: async name =>
        (await User.where({ name }).countDocuments()) === 0,
      message: () => "This name is already in use, try signin"
    }
  },
  password: String
});

userSchema.pre("save", async function() {
  if (this.isModified("password")) {
    this.password = await hash(this.password, 10);
  }
});

userSchema.methods.checkPassword = function(password) {
  return compare(password, this.password);
};

const User = mongoose.model("User", userSchema);

export default User;
