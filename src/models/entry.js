import mongoose, { Schema } from "mongoose";

const entrySchema = new Schema({
  model: {
    type: String,
    required: true
  },
  issue: {
    type: String,
    required: true
  },
  cust_name: String,
  cust_number: String,
  cust_price: Number,
  repair_cost: Number,
  is_delivered: {
    type: Boolean,
    required: true,
    default: false
  },
  is_repaired: {
    type: Boolean,
    required: true,
    default: false
  },
}, {
  timestamps: true
});

const Entry = mongoose.model("Entry", entrySchema);

export default Entry;
