import Joi from "joi";

export default Joi.object().keys({
  name: Joi.string().required(),
  password: Joi.string()
    .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/)
    .required()
});
