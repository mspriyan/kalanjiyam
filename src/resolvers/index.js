import user from "./user";
import { default as entryResolvers } from "./entry";
import date from "./date";
export default [user, entryResolvers];
