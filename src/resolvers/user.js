import mongoose from "mongoose";
import Joi from "joi";
import { User } from "../models";
import { signUpvalidator } from "../validators";
import { UserInputError, AuthenticationError } from "apollo-server-express";
import { createToken } from "../auth";

const { SECRET } = process.env;

export default {
  Query: {
    me: (parent, args, { user }, info) => {
      return User.findById(user.id)
    }
  },

  Mutation: {
    signUp: async (parent, { input }, context, info) => {
      await Joi.validate(input, signUpvalidator);
      const user = User.create(input);
      return await createToken(user);
    },

    signIn: async (parent, { input }, context, info) => {
      const user = await User.findOne({ name: input.name });

      if (!user) {
        throw new AuthenticationError("Please check the name");
      }

      if (!(await user.checkPassword(input.password))) {
        throw new AuthenticationError("Please check the password");
      }
      return await createToken(user);
    }
  }
};
