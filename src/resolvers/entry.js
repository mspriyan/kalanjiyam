import { AuthenticationError } from "apollo-server-express";
import { Entry } from "../models";

export default {
  Query: {
    entryByName: async (parent, { name }, context, info) => {
      return await Entry.findOne({ name });
    },

    allEntries: _ => {
      return Entry.find({});
    }
  },

  Mutation: {
    createEntry: async (parent, { input }, context, info) => {
      return await Entry.create(input);
    },

    updateEntry: async (_, { id, input }, context, info) => {
      try {
        const result = await Entry.findByIdAndUpdate(id, input, { new: true });
        if (!result) {
          throw new Error("Not found");
        } else {
          return result;
        }
      } catch (error) {
        throw error;
      }
    },

    deleteEntry: async (_, { id }, context, info) => {
      try {
        const result = await Entry.findOneAndRemove({
          _id: id
        });
        if (!result) {
          throw new Error("Not found");
        } else {
          return true;
        }
      } catch (error) {
        throw error;
      }
    }
  }
};
