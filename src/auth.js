import { sign } from "jsonwebtoken";
const { SECRET } = process.env;

export const createToken = async user => {
  return await sign({ id: user.id, name: user.name }, SECRET, {
    expiresIn: "7 days"
  });
};
