import root from "./root";
import user from "./user";
import { default as entrySchema } from "./entry";

export default [root, user, entrySchema];
