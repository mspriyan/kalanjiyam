import { gql } from "apollo-server-express";

export default gql`
  extend type Query {
    me: User @user
  }

  extend type Mutation {
    signUp(input: UserInput!): String
    signIn(input: UserInput!): String
  }

  type User {
    id: ID!
    name: String!
  }

  input UserInput {
    name: String!
    password: String!
  }
`;
