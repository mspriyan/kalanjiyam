"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "signUpvalidator", {
  enumerable: true,
  get: function () {
    return _signUp.default;
  }
});

var _signUp = _interopRequireDefault(require("./signUp"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }