"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _apolloServerExpress = require("apollo-server-express");

var _default = _apolloServerExpress.gql`
  extend type Query {
    me: User @user
  }

  extend type Mutation {
    signUp(input: UserInput!): String
    signIn(input: UserInput!): String
  }

  type User {
    id: ID!
    name: String!
  }

  input UserInput {
    name: String!
    password: String!
  }
`;

exports.default = _default;