"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _apolloServerExpress = require("apollo-server-express");

var _default = _apolloServerExpress.gql`
  scalar Date

  extend type Query {
    entryByName(name: String!): Entry
    allEntries: [Entry]!
  }

  extend type Mutation {
    createEntry(input: EntryInput): Entry @user
    updateEntry(id: ID!, input: EntryInput): Entry @user
    deleteEntry(id: ID!): Boolean @user
  }

  type Entry {
    id: ID!
    model: String!
    issue: String!
    cust_name: String
    cust_number: String
    cust_price: Float
    repair_cost: Float
    is_repaired: Boolean!
    is_delivered: Boolean!
    createdAt: Date
  }

  input EntryInput {
    model: String!
    issue: String!
    cust_name: String
    cust_number: String
    cust_price: Float
    repair_cost: Float
    is_repaired: Boolean
    is_delivered: Boolean
  }
`;

exports.default = _default;