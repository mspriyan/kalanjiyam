"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _root = _interopRequireDefault(require("./root"));

var _user = _interopRequireDefault(require("./user"));

var _entry = _interopRequireDefault(require("./entry"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = [_root.default, _user.default, _entry.default];
exports.default = _default;