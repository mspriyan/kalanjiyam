"use strict";

var _express = _interopRequireDefault(require("express"));

var _apolloServerExpress = require("apollo-server-express");

var _mongoose = _interopRequireDefault(require("mongoose"));

var _cors = _interopRequireDefault(require("cors"));

var _expressJwt = _interopRequireDefault(require("express-jwt"));

var _typeDefs = _interopRequireDefault(require("./typeDefs"));

var _resolvers = _interopRequireDefault(require("./resolvers"));

var _directives = _interopRequireDefault(require("./directives"));

var _os = require("os");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const {
  SECRET,
  NODE_ENV,
  PORT,
  DB_USERNAME,
  DB_PASSWORD,
  DB_HOST,
  DB_PORT,
  DB_NAME
} = process.env;
const app = (0, _express.default)();
app.use((0, _cors.default)());
app.disable("x-powered-by");
const auth = (0, _expressJwt.default)({
  secret: SECRET,
  credentialsRequired: false
});
app.use(auth);

(async () => {
  try {
    await _mongoose.default.connect(`mongodb://${DB_USERNAME}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}`, {
      useNewUrlParser: true
    });
    const server = new _apolloServerExpress.ApolloServer({
      // These will be defined for both new or existing servers
      typeDefs: _typeDefs.default,
      resolvers: _resolvers.default,
      context: ({
        req
      }) => {
        const user = req.user;
        return {
          user
        };
      },
      schemaDirectives: _directives.default,
      playground: true,
      introspection: true
    });
    server.applyMiddleware({
      app
    }); // app is from an existing express app

    app.get("/", function (req, res) {
      res.redirect("/graphql");
    });
    app.listen({
      port: PORT
    }, () => console.log(`Server ready is ready at http://${_os.hostname}:${PORT}${server.graphqlPath}`));
  } catch (error) {
    console.log(error);
  }
})();