"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mongoose = _interopRequireWildcard(require("mongoose"));

var _bcryptjs = require("bcryptjs");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

const userSchema = new _mongoose.Schema({
  name: {
    type: String,
    validate: {
      validator: async name => (await User.where({
        name
      }).countDocuments()) === 0,
      message: () => "This name is already in use, try signin"
    }
  },
  password: String
});
userSchema.pre("save", async function () {
  if (this.isModified("password")) {
    this.password = await (0, _bcryptjs.hash)(this.password, 10);
  }
});

userSchema.methods.checkPassword = function (password) {
  return (0, _bcryptjs.compare)(password, this.password);
};

const User = _mongoose.default.model("User", userSchema);

var _default = User;
exports.default = _default;