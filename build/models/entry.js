"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mongoose = _interopRequireWildcard(require("mongoose"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

const entrySchema = new _mongoose.Schema({
  model: {
    type: String,
    required: true
  },
  issue: {
    type: String,
    required: true
  },
  cust_name: String,
  cust_number: String,
  cust_price: Number,
  repair_cost: Number,
  is_delivered: {
    type: Boolean,
    required: true,
    default: false
  },
  is_repaired: {
    type: Boolean,
    required: true,
    default: false
  }
}, {
  timestamps: true
});

const Entry = _mongoose.default.model("Entry", entrySchema);

var _default = Entry;
exports.default = _default;