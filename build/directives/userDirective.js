"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _apolloServerExpress = require("apollo-server-express");

var _graphql = require("graphql");

class UserDirective extends _apolloServerExpress.SchemaDirectiveVisitor {
  visitFieldDefinition(field) {
    const {
      resolve = _graphql.defaultFieldResolver
    } = field;

    field.resolve = async function (...args) {
      const [,, context] = args;

      if (!context.user) {
        throw new _apolloServerExpress.AuthenticationError("You must login first.");
      } else {
        return await resolve.apply(this, args);
      }
    };
  }

}

var _default = UserDirective;
exports.default = _default;