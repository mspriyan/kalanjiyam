"use strict";

const db = require("mongoose");

const assert = require("assert");

const Schema = db.Schema;
db.connect("mongodb://localadm:localpass@localhost:27017/whitedb", {
  useNewUrlParser: true
});
const userSchema = new Schema({
  name: String,
  email: {
    type: String,
    validate: {
      validator: async email => (await User.where({
        email
      }).countDocuments()) === 0,
      message: () => "This email-id is already in use, try signin"
    }
  },
  password: String
}, {
  timestamps: true,
  discriminatorKey: "role"
});
const User = db.model("User", userSchema);
const clientSchema = new Schema({
  isPaid: {
    type: Boolean,
    default: false
  }
});
const Client = User.discriminator("Client", clientSchema);
const employeeSchema = new Schema({
  employeeID: String
});
const Employee = User.discriminator("Employee", employeeSchema);
var users = [{
  name: "Test",
  email: "hello@saod.com",
  password: "heman12"
}, {
  name: "Test12",
  email: "hello12@saod.com",
  password: "heman12",
  role: "Client"
}, {
  name: "Test13",
  email: "hello13@saod.com",
  password: "heman12",
  role: "Employee"
}];
User.create(users, function (error, users) {
  assert.ifError(error);
  assert.ok(users[0] instanceof User);
  assert.ok(users[1] instanceof Client);
  assert.ok(users[2] instanceof Employee);
  return "success";
});