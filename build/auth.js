"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createToken = void 0;

var _jsonwebtoken = require("jsonwebtoken");

const {
  SECRET
} = process.env;

const createToken = async user => {
  return await (0, _jsonwebtoken.sign)({
    id: user.id,
    name: user.name
  }, SECRET, {
    expiresIn: "7 days"
  });
};

exports.createToken = createToken;