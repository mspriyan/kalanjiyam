"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _apolloServerExpress = require("apollo-server-express");

var _models = require("../models");

var _default = {
  Query: {
    entryByName: async (parent, {
      name
    }, context, info) => {
      return await _models.Entry.findOne({
        name
      });
    },
    allEntries: _ => {
      return _models.Entry.find({});
    }
  },
  Mutation: {
    createEntry: async (parent, {
      input
    }, context, info) => {
      return await _models.Entry.create(input);
    },
    updateEntry: async (_, {
      id,
      input
    }, context, info) => {
      try {
        const result = await _models.Entry.findByIdAndUpdate(id, input, {
          new: true
        });

        if (!result) {
          throw new Error("Not found");
        } else {
          return result;
        }
      } catch (error) {
        throw error;
      }
    },
    deleteEntry: async (_, {
      id
    }, context, info) => {
      try {
        const result = await _models.Entry.findOneAndRemove({
          _id: id
        });

        if (!result) {
          throw new Error("Not found");
        } else {
          return true;
        }
      } catch (error) {
        throw error;
      }
    }
  }
};
exports.default = _default;