"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _joi = _interopRequireDefault(require("joi"));

var _models = require("../models");

var _validators = require("../validators");

var _apolloServerExpress = require("apollo-server-express");

var _auth = require("../auth");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const {
  SECRET
} = process.env;
var _default = {
  Query: {
    me: (parent, args, {
      user
    }, info) => {
      return _models.User.findById(user.id);
    }
  },
  Mutation: {
    signUp: async (parent, {
      input
    }, context, info) => {
      await _joi.default.validate(input, _validators.signUpvalidator);

      const user = _models.User.create(input);

      return await (0, _auth.createToken)(user);
    },
    signIn: async (parent, {
      input
    }, context, info) => {
      const user = await _models.User.findOne({
        name: input.name
      });

      if (!user) {
        throw new _apolloServerExpress.AuthenticationError("Please check the name");
      }

      if (!(await user.checkPassword(input.password))) {
        throw new _apolloServerExpress.AuthenticationError("Please check the password");
      }

      return await (0, _auth.createToken)(user);
    }
  }
};
exports.default = _default;